/* @flow */
/**
 * @copyright (c) 2017-present rafallesniak24@gmail.com
 */
import * as React from 'react';
import MoviesList from 'Components/MoviesList';

import Api from 'Modules/api';

import type { MovieApiResponseTypes } from 'Modules/types/MovieApiResponseTypes';

type PropsType = {
  query: string,
};

type StateType = {
  results: ?MovieApiResponseTypes,
  isProcessing: boolean,
};

class MoviesListContainer extends React.Component<PropsType, StateType> {
  state = {
    results: undefined,
    isProcessing: false,
  };

  componentWillReceiveProps(newProps: PropsType) {
    if (newProps.query.length) this.callQuery(newProps.query);
  }

  shouldComponentUpdate(nextProps: PropsType, nextState: StateType) {
    return (
      this.props.query.length !== nextProps.query.length ||
      this.state.isProcessing !== nextState.isProcessing
    );
  }

  callQuery = async (query: string) => {
    this.setState({ isProcessing: true });
    const resp: MovieApiResponseTypes = await Api.get('search/movie', { query });

    this.setState({
      results: resp,
      isProcessing: false,
    });
  };

  render() {
    const { results, isProcessing } = this.state;

    if (isProcessing) {
      return 'LOADING';
    }

    if (!results) {
      return 'Search above';
    }

    return <MoviesList moviesData={results} query={this.props.query} api={Api} />;
  }
}

export default MoviesListContainer;
