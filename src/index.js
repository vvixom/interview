/**
 * Application entry point.
 */

import React from 'react';
import ReactDOM from 'react-dom';

import { AppContainer } from 'react-hot-loader';

import App from 'Components/App';

/**
 * Render application in root element.
 * @returns {ReactElement} Application tree.
 */
const render = () => {
  ReactDOM.render(
    <AppContainer>
      <App />
    </AppContainer>,
    document.getElementById('root'),
  );
};

/**
 * Application start
 */
render();
if (module.hot) module.hot.accept('Components/App', () => render());
