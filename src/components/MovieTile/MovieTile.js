// @flow

import * as React from 'react';

import cx from 'Utils/classnames';

import './MovieTile.scss';

type PropsType = {
  title: string,
  adult: boolean,
  releaseDate: string,
  voteAverage: number,
  poster: ?string,
  movieId: number,
  onClick: (id: number) => void,
};

type StateType = {
  query: string,
};

class MovieTile extends React.Component<PropsType, StateType> {
  handleMovieClick = () => {
    this.props.onClick(this.props.movieId);
  };

  render() {
    const className = cx('tile', { 'tile--adult': this.props.adult });

    return (
      <div className={className} onClick={this.handleMovieClick}>
        <div className="tile__poster">
          {this.props.poster && <img src={`https://image.tmdb.org/t/p/w500/${this.props.poster}`} alt="poster" />}
        </div>
        <h1 className="tile__title">{this.props.title}</h1>
      </div>
    );
  }
}

export default MovieTile;
