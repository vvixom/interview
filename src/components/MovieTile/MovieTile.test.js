import React from 'react';
import { shallow } from 'enzyme';

import MovieTile from './MovieTile';

const defaultProps = {
  title: 'Title',
  adult: false,
  releaseDate: '2010-12-12',
  voteAverage: 8.8,
  poster: 'url',
  movieId: 123,
  onClick: () => {},
};

describe('<MovieTile />', () => {
  it('renders without crash', () => {
    const component = shallow(<MovieTile {...defaultProps} />);

    expect(component).toMatchSnapshot();
  });

  it('renders with custom classname', () => {
    const component = shallow(<MovieTile {...defaultProps} adult />);
    expect(component).toMatchSnapshot();
  });

  it('must call onClick handler', () => {
    const onClickMock = jest.fn();
    const component = shallow(<MovieTile {...defaultProps} onClick={onClickMock} />);

    component.find('.tile').simulate('click');

    expect(onClickMock).toBeCalledWith(123);
  });
});
