// @flow

import * as React from 'react';

import './MovieDetails.scss';

type PropsType = {
  movie: ?Object,
  onClose: () => void,
};

/**
 * Please forgive me - but its only presentation layer, my time is running out :(
 */

const MovieDetails = ({ movie, onClose }: PropsType) =>
  movie && (
    <div className="movie-details">
      <div className="movie-details__close" onClick={onClose}>
        X
      </div>
      {Object.keys(movie).map((k, i) => (
        <div key={i}>
          {k} - {movie[k]}
        </div>
      ))}
    </div>
  );

export default MovieDetails;
