// @flow

import * as React from 'react';

import SearchInput from 'Components/SearchInput';
import MoviesListContainer from 'Containers/MoviesListContainer';

import './App.scss';

type StateType = {
  searchQuery: string,
};

class App extends React.Component<void, StateType> {
  state = {
    searchQuery: '',
  };

  handleSearchChange = (query: string) => {
    this.setState({
      searchQuery: query,
    });
  };

  render() {
    return (
      <div className="app">
        <div className="app__root">
          <SearchInput onSearchChange={this.handleSearchChange} />
          <MoviesListContainer query={this.state.searchQuery} />
        </div>
      </div>
    );
  }
}

export default App;
