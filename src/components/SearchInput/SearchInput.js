// @flow

import * as React from 'react';

import './SearchInput.scss';

type PropsType = {
  onSearchChange: (query: string) => void,
};

type StateType = {
  query: string,
};

class SearchInput extends React.Component<PropsType, StateType> {
  state = {
    query: '',
  };

  handleEnter = (e: SyntheticInputEvent<>) => {
    if (e.keyCode === 13) {
      this.props.onSearchChange(this.state.query);
    }
  };

  handleQueryChange = (e: SyntheticInputEvent<>) => {
    const query = e.target.value;

    this.setState({
      query,
    });
  };

  render() {
    return (
      <div className="search-input">
        <input
          onKeyDown={this.handleEnter}
          onChange={this.handleQueryChange}
          placeholder="Type your query"
          value={this.state.query}
        />
      </div>
    );
  }
}

export default SearchInput;
