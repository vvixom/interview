// @flow

import * as React from 'react';
import MovieTile from 'Components/MovieTile';
import MovieModal from 'Components/MovieModal';
import MovieDetails from 'Components/MovieDetails';
import withPagination from 'Modules/hoc/withPagination';

import type { MovieApiResponseTypes } from 'Modules/types/MovieApiResponseTypes';

type PropsType = {
  moviesData: MovieApiResponseTypes,
  currentPage: number,
};

type StateType = {
  isModalVisible: boolean,
  activeModalMovieId: ?number,
};

class MoviesList extends React.Component<PropsType, StateType> {
  state = {
    isModalVisible: false,
    activeModalMovieId: null,
  };

  handleMovieOnClick = (id: number) => {
    this.setState({ isModalVisible: true, activeModalMovieId: id });
    window.scrollTo(0, 0);
  };

  handleModalClose = () => {
    this.setState({
      isModalVisible: false,
      activeModalMovieId: null,
    });
  };

  findMovie(): ?Object {
    return this.props.moviesData.results.find(movie => movie.id === this.state.activeModalMovieId);
  }

  render() {
    const { moviesData, currentPage } = this.props;
    if (moviesData.results.length === 0) {
      return <h1>No results</h1>;
    }

    return (
      <div className="movies-list">
        <div className="movies-list__details">
          Showing {moviesData.results.length * currentPage} of {moviesData.total_results} total results | {currentPage} /{' '}
          {moviesData.total_pages} page
        </div>
        {moviesData.results.map(movie => (
          <MovieTile
            key={movie.id}
            title={movie.title}
            poster={movie.poster_path}
            releaseDate={movie.release_date}
            adult={movie.adult}
            voteAverage={movie.vote_average}
            movieId={movie.id}
            onClick={this.handleMovieOnClick}
          />
        ))}
        {this.state.isModalVisible && (
          <MovieModal>
            <MovieDetails movie={this.findMovie()} onClose={this.handleModalClose} />
          </MovieModal>
        )}
      </div>
    );
  }
}

export default withPagination(MoviesList);
