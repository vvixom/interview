// @flow

import * as React from 'react';
import ReactDOM from 'react-dom';

import './MovieModal.scss';

type PropsType = {
  children: React.Node,
};

const modalRoot = document.getElementById('modal-root');

class MovieModal extends React.Component<PropsType> {
  constructor(props: PropsType) {
    super(props);

    this.domEl = document.createElement('div');
  }

  componentDidMount() {
    if (modalRoot) modalRoot.appendChild(this.domEl);
  }

  componentWillUnmount() {
    if (modalRoot) modalRoot.removeChild(this.domEl);
  }

  domEl: HTMLDivElement;

  render() {
    return ReactDOM.createPortal(
      <div className="movie-modal"><div className="movie-modal__content">{this.props.children}</div></div>,
      this.domEl,
    );
  }
}

export default MovieModal;
