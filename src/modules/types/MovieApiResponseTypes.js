// @flow

export type ResultsType = {
  adult: boolean,
  title: string,
  release_date: string,
  original_title: string,
  id: number,
  poster_path: ?string,
  popularity: number,
  vote_average: number,
}

export type MovieApiResponseTypes = {
  page: number,
  results: Array<ResultsType>,
  total_pages: number,
  total_results: number,
}
