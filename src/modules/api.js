// @flow

import { encodeUriParameters } from 'Utils/helpers';

type QueryConfigType = {
  params?: Object,
};

const Api = () => {
  const url = 'https://api.themoviedb.org/3';
  const apiParam = { api_key: API_KEY };

  const request = async (uri: string, config?: QueryConfigType = {}): Promise<Response> => {
    const params = {
      ...apiParam,
      ...config.params,
    };

    try {
      const resp = await fetch(`${uri}?${encodeUriParameters(params)}`, {
        ...config,
      });

      return await resp.json();
    } catch (e) {
      return Promise.reject(e);
    }
  };

  return {
    get: async function get<T>(endpoint: string, params: Object): Promise<T & Response> {
      const resp = await request(`${url}/${endpoint}`, {
        method: 'GET',
        params,
      });

      return resp;
    },
  };
};

export default Api();
