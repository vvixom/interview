import * as React from 'react';

import cx from 'Utils/classnames';

import './withPagination.scss';

type StateType = {
  moviesData: Object,
  page: number,
  isLoading: boolean,
};

function withPagination<Props>(Component: React.ComponentType<Props>): React.ComponentType<Props> {
  return class extends React.Component<Props, StateType> {
    constructor(props: Props) {
      super(props);

      this.state = {
        moviesData: props.moviesData,
        page: props.moviesData.page,
        isLoading: false,
      };
    }

    callQuery = async (page: number) => {
      this.setState({ isLoading: true });

      const resp = await this.props.api.get('search/movie', {
        query: this.props.query,
        page,
      });

      this.setState({ moviesData: resp, isLoading: false, page });
    }

    handlePrev = () => {
      this.callQuery(this.state.page - 1);
    };

    handleNext = () => {
      this.callQuery(this.state.page + 1);
    };

    render() {
      const { moviesData, page, isLoading } = this.state;

      return (
        <div className="pagination">
          <div className={cx('pagination__view', { 'pagination--processing': isLoading })}>
            <Component moviesData={moviesData} currentPage={page} />
          </div>
          <div className="pagination__controls">
            {page > 1 && <button onClick={this.handlePrev}>Prev</button>}
            {page < moviesData.total_pages && <button onClick={this.handleNext}>Next</button>}
          </div>
        </div>
      );
    }
  };
}

export default withPagination;
