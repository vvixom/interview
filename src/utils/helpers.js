// @flow

export const encodeUriParameters = (data: Object): string =>
  Object.keys(data).map(k => [k, data[k]].map(encodeURIComponent).join('=')).join('&')
