// @flow

const cx = (...args: Array<string | Object>) => {
  const result = [];
  args.forEach((arg) => {
    if (typeof arg === 'string') {
      result.push(arg);
    } else if (typeof arg === 'object') {
      Object.keys(arg).forEach((o) => {
        if (arg[o] === true) {
          result.push(o);
        }
      });
    }
  });

  return result.join(' ');
};

export default cx;
