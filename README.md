# React movie DB
This is simple project for a recruting assignment.

# Usage
First of all you have to obtain your API key from themoviedb.org, paste it to `config.js.example` and rename to `config.js`

# Installation and project runing
Use npm or yarn to install project dependencies for eg. command `yarn`. Next start development mode by using for eg. command: `npm run dev` or `yarn dev`

# Testing
Just run `yarn test` and lint by `yarn lint`. Flow typings you can check by running `yarn flow`

# Constraint
This project isn't perfect, even isn't good. I hope it is showing my philosophy and a way of thinking in React and JS in general.
Code is well documented by Flow typings
